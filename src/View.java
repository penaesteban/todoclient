import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;



public class View {
	
	
	protected Label LblTitel;
	private Stage stage;
	private Model model;
	protected TextField  txtPort;
	protected Label LblPort;
	protected TextArea txtOutput;
	protected Label LblMessage;
	protected TextField txtMessage;
	protected Button btnSenden;
	protected Label LblIpAdresse;
	protected TextField txtIpAdresse;
	protected Button btnVerbinden;
	
	
	public View(Stage stage, Model model) {
		
		this.stage = stage;
		this.model = model;
		
		
		LblTitel = new Label("ToDo Client");
		txtPort = new TextField ("50002");
		LblPort = new Label("Port");
		txtOutput = new TextArea("");
		txtMessage = new TextField("");
		btnSenden = new Button("Senden");
		LblMessage = new Label("Nachricht");
		LblIpAdresse = new Label("IP Adresse");
		txtIpAdresse = new TextField("127.0.0.1");
		btnVerbinden = new Button("Connect");
		
		GridPane root = new GridPane();
		root.add(LblTitel, 0, 1);
		root.add(LblIpAdresse,0,2);
		root.add(txtIpAdresse, 0,3);
		root.add(LblPort, 0,4);
		root.add(txtPort, 0,5);
		root.add(btnVerbinden,0,6);
		root.add(txtOutput, 0,7);
		root.add(LblMessage,0,8);
		root.add(txtMessage, 0,9 );
		root.add(btnSenden, 0,10);
		
		
		
		
		Scene scene = new Scene(root);
		
		stage.setScene(scene);
		stage.setHeight(800);
		stage.setWidth(800);
		
	
	
				
	}

	public void  start() {
		stage.show();
		
	}
	
	public Stage getStage() {
		return stage;
	
	}

	
	
}
