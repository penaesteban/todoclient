

public class Controller {
	private Model model;
	private View view;

	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;

		view.btnSenden.setOnAction(e ->{
			String message= view.txtMessage.getText();
			model.send(message);
			view.txtOutput.setText(view.txtOutput.getText() + message + "\n");
			view.txtMessage.setText("");
		});
		
		model.newestMessage.addListener( (o, oldValue, newValue) -> {
			if (!newValue.isEmpty()) // Ignore empty messages
				view.txtOutput.appendText(newValue + "\n");
		} );
		
		
		view.btnVerbinden.setOnAction(e->{
			
			int port = Integer.parseInt(view.txtPort.getText());
			String ipAdresse = view.txtIpAdresse.getText();
			
			model.connect(ipAdresse,port);
		});
	}
}
