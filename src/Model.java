import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import javafx.beans.property.SimpleStringProperty;

public class Model {

	protected SimpleStringProperty newestMessage = new SimpleStringProperty();
	
	private ServerSocket listener;
	private Socket socket;
	
	private String ipAdresse;
	private int port;
	
	public void connect(String ipAdresse, int port) {
		
		this.ipAdresse = ipAdresse;
		this.port = port;
		
		try {
			socket = new Socket(ipAdresse, port);
		} catch (Exception e) {
			
		}
		
		
		Runnable r = new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
						String msgText = in.readLine(); 
						newestMessage.set(""); 
						newestMessage.set(msgText);
					}
					catch(Exception e){
						
					}
					
				}
			}
		};
		
		Thread t = new Thread(r);
		t.start();
	}
	
	public void send(String message) {
		
		try {
			OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());
			out.write(message + "\n");
			out.flush();
		}
		catch(Exception e) {
			
		}
		
	}
	
	
}

