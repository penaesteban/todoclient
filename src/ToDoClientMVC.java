
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;


public class ToDoClientMVC  extends Application{

	private Model model;
	private View view;
	private Controller controller;
	
		
		

		public static void main(String[] args) {
			launch(args);

		}
		
		@Override 
		public void start ( Stage primaryStage) {
			
			model = new Model();
			view = new View(primaryStage,model);
			controller = new Controller(model, view);
			
			
			view.start();
				
		}

	
	
	
}





