# ToDoClient

Dieses Projekt gehört zum Miniprojekt Nr.2 beim Modul Software Engineering.
**Dieses Programm wurde als Client mit GUI (JavaFX)erarbeitet, sodass die Antworten des Servers nicht in der Konsole angezeigt werden**

Das Programm wurde als Einzelarbeit durch Esteban Andres Pena Zhina programmiert.

**Funktionalität:**

- Server (127.0.0.1)und Port (50002)werden beim Start bereits mitgegeben. Wichtig ist es danach auf "Connect" , um mit dem Server zu          kommunizieren.
- MessagTypen können im TextFeld gesendet werden und mit dem Button "Senden" an den Server geschickt werden.
- Die Antwort des Servers werden dann im Textfeld angezeigt.


**MessageTypen ( Client -> Server)**

- **MessageType**: Ping ; **Erwartetes Ergebnis**: Result|True
- **MessageType**: CreateLogin|username|password ; **Erwartetes Ergebnis**: Result|True
- **MessageType**: Login|username|password ; **Erwartetes Ergebnis**: Result|True|Token => vom Server generiert
- **MessageType**: Logout ; **Erwartetes Ergebnis**: Result|True

**To-Do MessageTypes**

- **MessageType + Eingabe von Client**: CreateToDo|Token vom Server|Titel|Prority|Beschreibung ; **Erwartetes Ergebnis**: Result|True
- **MessageType + Eingabe von Client**: GetToDo|Token vom Server|index ; **Erwartetes Ergebnis**: Result|True|index|Titel|Priority|Beschreibung
- **MessageType + Eingabe von Client**: DeleteToDo|index der ToDo Notiz ; **Erwartetes Ergebnis**: Result|True
- **MessageType + Eingabe von Client**: ListToDos  ; **Erwartetes Ergebnis**: Result|Lists of Index


**Beim Crash oder Einfrieren des Clients:**
Server und Client schliessen und neustarten
